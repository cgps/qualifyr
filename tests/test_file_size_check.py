from unittest import TestCase
import os, yaml
from qualifyr.file_size_check_file import FileSizeCheckFile


class TestFileSizeCheckFile(TestCase):
    def setUp(self):
        pass_file_size_check_path = '{0}/test_data/file_size_check_pass.tsv'.format(os.path.dirname(os.path.realpath(__file__)))
        fail_file_size_check_path = '{0}/test_data/file_size_check_fail.tsv'.format(os.path.dirname(os.path.realpath(__file__)))
        self.invalid_file_size_check_path = '{0}/test_data/file_size_check_invalid.tsv'.format(os.path.dirname(os.path.realpath(__file__)))
        self.pass_file_size_check_file = FileSizeCheckFile(pass_file_size_check_path)
        self.fail_file_size_check_file = FileSizeCheckFile(fail_file_size_check_path)

        pass_conditions_file = '{0}/test_data/pass_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(pass_conditions_file) as pass_conditions_yml:
            self.pass_conditions = yaml.load(pass_conditions_yml)

    def test_file_size_check_validation(self):
        self.assertEqual(2, len(self.pass_file_size_check_file.validate()[0]))
        self.assertEqual(2, len(self.pass_file_size_check_file.validate()[1]))
    
    def test_file_size_check_invalidation(self):
        with self.assertRaises(Exception): FileSizeCheckFile(self.invalid_file_size_check_path)

    def test_file_size_check_parsing(self):
        self.assertEqual(
            {
                'file': 'GCF_000019645.1_ASM1964v1',
                'size': 35.2
            },
            self.pass_file_size_check_file.metrics
        )

    def test_file_size_check_pass(self):
        self.pass_file_size_check_file.check(self.pass_conditions)
        self.assertEqual('PASS', self.pass_file_size_check_file.overall_qc_check_result())

        self.assertEqual(
            [('size', 35.2, '< 15', 'PASS')],
            [quality_check.to_tuple() for quality_check in self.pass_file_size_check_file.quality_checks]
        )

        self.assertEqual(
            [],
            self.pass_file_size_check_file.failed_checks
        )

        self.assertEqual('PASS', self.pass_file_size_check_file.overall_qc_check_result())
    
    def test_bactinspector_warning(self):
        self.fail_file_size_check_file.check(self.pass_conditions)
        self.assertEqual(
             [('size', 10.3, '< 15', 'FAILURE')],
            [quality_check.to_tuple() for quality_check in self.fail_file_size_check_file.quality_checks]
        )

        self.assertEqual(
            [('size', 10.3, '< 15', 'FAILURE')],
            [quality_check.to_tuple() for quality_check in self.fail_file_size_check_file.failed_checks]
        )
        self.assertEqual('FAILURE', self.fail_file_size_check_file.overall_qc_check_result())