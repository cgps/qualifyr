from unittest import TestCase
import os, tempfile
from qualifyr.run_qualifyr import parse_arguments, run_check_command, run_report_command, choose_command

class TestRunQualiFyr(TestCase):
    def setUp(self):
        self.parser = parse_arguments()

        self.fail_conditions_file = '{0}/test_data/fail_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        self.pass_conditions_file = '{0}/test_data/pass_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        self.quast_path = '{0}/test_data/quast_valid.txt'.format(os.path.dirname(os.path.realpath(__file__)))
        self.fastqc_path = '{0}/test_data/fastqc_valid.txt'.format(os.path.dirname(os.path.realpath(__file__)))
        self.confindr_pass_path = '{0}/test_data/confindr_pass.csv'.format(os.path.dirname(os.path.realpath(__file__)))
        self.confindr_fail_path = '{0}/test_data/confindr_fail.csv'.format(os.path.dirname(os.path.realpath(__file__)))
        self.bactinspector_pass_path = '{0}/test_data/bactinspector_good.tsv'.format(os.path.dirname(os.path.realpath(__file__)))
        self.bactinspector_uncertain_path = '{0}/test_data/bactinspector_uncertain.tsv'.format(os.path.dirname(os.path.realpath(__file__)))
 
        self.report_inputs_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test_data', 'report_inputs')

    def test_arg_parsing_fails_with_missing_params(self):
        with self.assertRaises(Exception):
            self.parser.parse_args(['check'])

        with self.assertRaises(Exception):
            args = self.parser.parse_args(['check', '-y', self.fail_conditions_file, '-s', 'sample_name'])
            run_check_command(self.parser, args)
        
        with self.assertRaises(Exception):
            self.parser.parse_args(['report'])
        
    def test_arg_parsing_succeeds_with_correct_params(self):
        try:
            self.parser.parse_args(['check', '-y', self.fail_conditions_file, '-s', 'sample_name'])
        except Exception:
            self.fail("parse_args() raised an Exception unexpectedly!")

        try:
            args = self.parser.parse_args(['report', '-i', '.'])
        except Exception:
            self.fail("parse_args() raised an Exception unexpectedly!")

        # check no exception is raised if at least one quality file is specified
        try:
            args = self.parser.parse_args(['check', '-y', self.fail_conditions_file, '-s', 'sample_name', '-q', self.quast_path ])
            run_check_command(self.parser, args)
        except Exception:
            self.fail("run_check_command() raised an Exception unexpectedly!")
    
    def test_check_tsv_command_output(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            args = self.parser.parse_args(['check', '-y', self.fail_conditions_file, '-s', 'sample1', '-q', self.quast_path , '-o', tmpdir])
            run_check_command(self.parser, args)
            with open(os.path.join(tmpdir, 'sample1.qualifyr.tsv')) as qualifyr_output:
                output_string = qualifyr_output.read()
                self.assertIn('file_type\tmetric_name\tmetric_value\tcheck\tcheck result\n', output_string)
                self.assertIn('quast\t# contigs (>= 1000 bp)\t48\t> 40\tWARNING\n', output_string)
                self.assertIn('quast\tN50\t146240\t< 150000\tWARNING\n', output_string)

            args = self.parser.parse_args(['check', '-y', self.pass_conditions_file,
                            '-s', 'sample2', '-q', self.quast_path ,
                            '-f', self.fastqc_path ,'-c', self.confindr_pass_path ,
                            '-b', self.bactinspector_pass_path,'-o', tmpdir])
            run_check_command(self.parser, args)
            with open(os.path.join(tmpdir, 'sample2.qualifyr.tsv')) as qualifyr_output:
                output_string = qualifyr_output.read()
                self.assertIn('file_type\tmetric_name\tmetric_value\tcheck\tcheck result\n', output_string)
                self.assertNotIn('quast\t# contigs (>= 1000 bp)\t48\t> 40\tWARNING\n', output_string)

            args = self.parser.parse_args(['check', '-y', self.fail_conditions_file, '-s', 'sample3', '-q', self.quast_path ,
                '-f', self.fastqc_path ,'-c', self.confindr_fail_path ,'-b', self.bactinspector_uncertain_path, '-o', tmpdir])
            run_check_command(self.parser, args)
            with open(os.path.join(tmpdir, 'sample3.qualifyr.tsv')) as qualifyr_output:
                output_string = qualifyr_output.read()
                self.assertIn('file_type\tmetric_name\tmetric_value\tcheck\tcheck result\n', output_string)
                self.assertIn('quast\t# contigs (>= 1000 bp)\t48\t> 40\tWARNING\n', output_string)
                self.assertIn('fastqc 1\tPer tile sequence quality\tFAIL\tEquals FAIL\tFAILURE\n', output_string)
                self.assertIn('confindr\tcontam_status\tTrue\tEquals True\tFAILURE\n', output_string)
                self.assertIn('bactinspector\tresult\tuncertain\tEquals uncertain\tWARNING\n', output_string)
                    
    def test_check_json_command_output(self):
        with tempfile.TemporaryDirectory() as tmpdir:      
            # JSON for failing sample  format
            args = self.parser.parse_args(['check', '-y', self.fail_conditions_file, '-s', 'sample1', '-q', self.quast_path , '-o', tmpdir, '-j'])
            run_check_command(self.parser, args)
            with open(os.path.join(tmpdir, 'sample1.qualifyr.json')) as qualifyr_output:
                output_string = qualifyr_output.read()
                self.assertIn('"sample_name": "sample1"', output_string)
                self.assertIn('"quast": {\n      "# contigs (>= 1000 bp)": {\n        "metric_value": 48,\n        "check": "> 40",\n        "check_result": "WARNING"\n      }', output_string)
                self.assertIn('"N50": {\n        "metric_value": 146240,\n        "check": "< 150000",\n        "check_result": "WARNING"\n      }', output_string)
            
            # test full output for passing file
            args = self.parser.parse_args(['check', '-y', self.pass_conditions_file, '-s', 'sample1', '-q', self.quast_path ,
                '-f', self.fastqc_path ,'-c', self.confindr_pass_path ,'-b', self.bactinspector_pass_path, '-o', tmpdir, '-j'])
            run_check_command(self.parser, args)

            with open(os.path.join(tmpdir, 'sample1.qualifyr.json')) as qualifyr_output:
                output_string = qualifyr_output.read()
                self.assertIn('"sample_name": "sample1"', output_string)
                self.assertIn('"quast": {\n      "# contigs (>= 1000 bp)": {\n        "metric_value": 48,\n        "check": "> 70",\n        "check_result": "PASS"\n      }', output_string)
                self.assertIn('"N50": {\n        "metric_value": 146240,\n        "check": "< 100000",\n        "check_result": "PASS"\n      }', output_string)
                self.assertIn('"fastqc 1": {\n      "Basic Statistics": {\n        "metric_value": "PASS",\n        "check": "Equals WARN",\n        "check_result": "PASS"\n      }', output_string)
                self.assertIn('"Per base N content": {\n        "metric_value": "WARN",\n        "check": "Equals FAIL",\n        "check_result": "PASS"\n      }', output_string)
                self.assertIn('"confindr": {\n      "contam_status": {\n        "metric_value": "False",\n        "check": "Equals True",\n        "check_result": "PASS"\n      }', output_string)
                self.assertIn('"bactinspector": {\n      "result": {\n        "metric_value": "good",\n        "check": "Equals uncertain",\n        "check_result": "PASS"\n      }', output_string)
                self.assertIn('"species": {\n        "metric_value": "Escherichia coli",\n        "check": "Equals ",\n        "check_result": "PASS"\n      }', output_string)
                self.assertIn('"extra_info": {\n    "quast": null,\n    "fastqc 1": null,\n    "confindr": "Salmonella. 0 % contamination. 0 contaminating SNVs.",\n    "bactinspector": null\n  }', output_string)


    def test_check_report_output(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            args = self.parser.parse_args(['report', '-i', self.report_inputs_dir, '-o', tmpdir])
            run_report_command(self.parser, args)
            with open(os.path.join(tmpdir, 'qualifyr_report.html')) as report_file:
                html_report_string = report_file.read()
                self.assertIn('"sample_name": "sample_2", "result": "FAILURE"', html_report_string)
                self.assertIn('"sample_name": "sample_1", "result": "PASS"', html_report_string)
                self.assertIn('{ title: "Sample Name", data: "sample_name"}, { title: "Result", data: "result" }', html_report_string)
                self.assertNotIn('{title: "# contigs (>= 1000 bp)", data: "checks.quast.# contigs (>= 1000 bp).metric_value" }', html_report_string)

            args = self.parser.parse_args(['report', '-i', self.report_inputs_dir, '-o', tmpdir, '-c', 'quast.N50,quast.# contigs (>= 1000 bp),confindr.contam_status'])
            run_report_command(self.parser, args)
            with open(os.path.join(tmpdir, 'qualifyr_report.html')) as report_file:
                html_report_string = report_file.read()
                self.assertIn('"sample_name": "sample_2", "result": "FAILURE"', html_report_string)
                self.assertIn('"sample_name": "sample_1", "result": "PASS"', html_report_string)
                self.assertNotIn('{ title: "Sample Name", data: "sample_name"}, { title: "Result", data: "result" }', html_report_string)
                self.assertIn('{ title: "Sample Name", data: "sample_name"}, {title: "N50", data: "checks.quast.N50.metric_value" }', html_report_string)
                self.assertIn('{title: "# contigs (>= 1000 bp)", data: "checks.quast.# contigs (>= 1000 bp).metric_value" }', html_report_string)
                self.assertIn('{title: "contam_status", data: "checks.confindr.contam_status.metric_value" }, { title: "Result", data: "result" }', html_report_string)
