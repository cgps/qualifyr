from unittest import TestCase
import os, tempfile, glob, json
from qualifyr.quality_file import QualityFile
from qualifyr.quast_file import QuastFile
from qualifyr.fastqc_summary_file import FastqcFile
from qualifyr.confindr_file import ConFindrFile
from qualifyr.file_size_check_file import FileSizeCheckFile
from qualifyr.conditions_file import import_yaml_file
from qualifyr.html_report import create_html_report
from qualifyr.text_report import create_text_report


class TestMultipleFiles(TestCase):
    def setUp(self):
        multiple_test_data_dir = os.path.join((os.path.dirname(os.path.realpath(__file__))), 'test_data', 'multiple_file_tests')
        sample_files = {}
        for sample_name in ['pass_sample', 'warning_sample', 'fail_sample', 'fail_contamination', 'fail_file_size_check_sample']:
            sample_files[sample_name] = {
                'fastqc1': os.path.join(multiple_test_data_dir, '{0}_R1_fastqc.txt'.format(sample_name)),
                'fastqc2': os.path.join(multiple_test_data_dir, '{0}_R2_fastqc.txt'.format(sample_name)),
                'quast': os.path.join(multiple_test_data_dir, '{0}_quast.txt'.format(sample_name)),
                'confindr': os.path.join(multiple_test_data_dir, '{0}_confindr.csv'.format(sample_name)),
                'file_size_check' : os.path.join(multiple_test_data_dir, '{0}_file_size_check.tsv'.format(sample_name))
            }


        self.sample_quality_files = {}
        for sample_name in ['pass_sample', 'warning_sample', 'fail_sample', 'fail_contamination']:
            self.sample_quality_files[sample_name] = [
                FastqcFile(sample_files[sample_name]['fastqc1'],1),
                FastqcFile(sample_files[sample_name]['fastqc2'],2),
                QuastFile(sample_files[sample_name]['quast']),
                ConFindrFile(sample_files[sample_name]['confindr']),
                FileSizeCheckFile(sample_files[sample_name]['file_size_check'])
            ]

        test_data_dir = os.path.join((os.path.dirname(os.path.realpath(__file__))), 'test_data')
        conditions_file = os.path.join(test_data_dir, 'pass_conditions.yml')
        conditions = import_yaml_file(conditions_file)


        # check each set of sample quality files
        for sample_name in ['pass_sample', 'warning_sample', 'fail_sample', 'fail_contamination']:
            self.sample_quality_files[sample_name] = QualityFile.check_multiple_files(self.sample_quality_files[sample_name] , conditions)

       
    def test_overall_qc_check_result(self):
        # test passing sample
        self.assertEqual('PASS', QualityFile.multiple_overall_qc_check_result(self.sample_quality_files['pass_sample']))

        # test warning sample
        self.assertEqual('WARNING', QualityFile.multiple_overall_qc_check_result(self.sample_quality_files['warning_sample']))

        # test failure sample
        self.assertEqual('FAILURE', QualityFile.multiple_overall_qc_check_result(self.sample_quality_files['fail_sample']))

        # test failure contamination sample
        self.assertEqual('FAILURE', QualityFile.multiple_overall_qc_check_result(self.sample_quality_files['fail_contamination']))

    def test_html_report_generation(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            # write out json files
            for sample_name in ['pass_sample', 'warning_sample', 'fail_sample', 'fail_contamination']:
                output_string = QualityFile.multiple_qc_result_string(sample_name, self.sample_quality_files[sample_name], output_format = 'json', only_failed_checks = False )
                with open(os.path.join(tmpdir, '{0}.qualifyr.json'.format(sample_name)), 'w') as outfile:
                    outfile.write(output_string)
            # create report
            combined_sample_qualifyr_list = []
            for json_file in glob.glob(os.path.join(tmpdir, '*.qualifyr.json')):
                with open(json_file) as json_fh:
                    combined_sample_qualifyr_list.append(json.load(json_fh))
            
            create_html_report(combined_sample_qualifyr_list, tmpdir)
            # check report file contents
            with open(os.path.join(tmpdir, 'qualifyr_report.html')) as report_file:
                html_report_string = report_file.read()
            self.assertIn('"sample_name": "fail_sample", "result": "FAILURE"', html_report_string)
            self.assertIn('"sample_name": "fail_contamination", "result": "FAILURE"', html_report_string)
            self.assertIn('"sample_name": "warning_sample", "result": "WARNING"', html_report_string)
            self.assertIn('"sample_name": "pass_sample", "result": "PASS"', html_report_string)
            self.assertIn('"N50": {"metric_value": 35000, "check": "< 50000", "check_result": "FAILURE"', html_report_string)
            self.assertIn('"fastqc 2": {"Basic Statistics": {"metric_value": "WARN", "check": "Equals WARN", "check_result": "WARNING"', html_report_string)
            self.assertIn('"contam_status": {"metric_value": "True", "check": "Equals True", "check_result": "FAILURE"', html_report_string)
            self.assertIn('"confindr": "Staphylococcus:Salmonella. 8.2 % contamination. 12 contaminating SNVs."', html_report_string)
    
    def test_text_report_generation(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            # write out json files
            for sample_name in ['pass_sample', 'warning_sample', 'fail_sample', 'fail_contamination']:
                output_string = QualityFile.multiple_qc_result_string(sample_name, self.sample_quality_files[sample_name], output_format = 'json', only_failed_checks = False )
                with open(os.path.join(tmpdir, '{0}.qualifyr.json'.format(sample_name)), 'w') as outfile:
                    outfile.write(output_string)
            # create report
            combined_sample_qualifyr_list = []
            for json_file in glob.glob(os.path.join(tmpdir, '*.qualifyr.json')):
                with open(json_file) as json_fh:
                    combined_sample_qualifyr_list.append(json.load(json_fh))
            
            create_text_report(combined_sample_qualifyr_list, tmpdir)
            # check report file contents
            with open(os.path.join(tmpdir, 'qualifyr_report.tsv')) as report_file:
                text_report_string = report_file.read()
            self.assertIn("sample_name\tresult\tconfindr.contam_status.metric_value\tconfindr.contam_status.check_result\tfastqc 1.Adapter Content.metric_value\tfastqc 1.Adapter Content.check_result", text_report_string)
            self.assertIn("pass_sample\tPASS\tFalse\tPASS\tPASS\tPASS\tPASS\tPASS\tPASS\tPASS\tWARN\tPASS\tWARN\tPASS\tPASS\tPASS\tPASS", text_report_string)
            self.assertIn("warning_sample\tWARNING\tFalse\tPASS\tPASS\tPASS\tPASS\tPASS\tPASS\tPASS\tWARN\tPASS\tWARN", text_report_string)
            self.assertIn("fail_sample\tFAILURE\tFalse\tPASS\tPASS\tPASS\tPASS\tPASS\tPASS\tPASS\tWARN\tPASS\tWARN\tPASS\tFAIL\tFAILURE", text_report_string)
            self.assertIn("fail_contamination\tFAILURE\tTrue", text_report_string)
            