from unittest import TestCase
import os, yaml
from qualifyr.fastqc_summary_file import FastqcFile


class TestFastqcFile(TestCase):
    def setUp(self):
        valid_fastqc_path = '{0}/test_data/fastqc_valid.txt'.format(os.path.dirname(os.path.realpath(__file__)))
        self.invalid_fastqc_path = '{0}/test_data/fastqc_invalid.txt'.format(os.path.dirname(os.path.realpath(__file__)))
        self.valid_fastqc_file = FastqcFile(valid_fastqc_path)

        fastqc_pass_conditions_file = '{0}/test_data/pass_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(fastqc_pass_conditions_file) as pass_conditions_yml:
            self.pass_conditions = yaml.load(pass_conditions_yml)
        
        fastqc_fail_conditions_file = '{0}/test_data/fail_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(fastqc_fail_conditions_file) as fail_conditions_yml:
            self.fail_conditions = yaml.load(fail_conditions_yml)

    def test_fastqc_validation(self):
        self.assertEqual(11, len(self.valid_fastqc_file.validate()))
    
    def test_fastqc_invalidation(self):
        with self.assertRaises(Exception): FastqcFile(self.invalid_fastqc_path)

    def test_fastqc_parsing(self):
        self.assertEqual(
            {
                'Basic Statistics': 'PASS',
                'Per base sequence quality': 'PASS',
                'Per tile sequence quality': 'FAIL',
                'Per sequence quality scores': 'PASS',
                'Per base sequence content': 'WARN',
                'Per sequence GC content': 'PASS',
                'Per base N content': 'WARN',
                'Sequence Length Distribution': 'WARN',
                'Sequence Duplication Levels': 'PASS',
                'Overrepresented sequences': 'PASS',
                'Adapter Content': 'PASS'
            },
            self.valid_fastqc_file.metrics
        )

    def test_fastqc_pass(self):
        self.valid_fastqc_file.check(self.pass_conditions)
        self.assertEqual('PASS', self.valid_fastqc_file.overall_qc_check_result())
        observed_quality_checks = [
            quality_check.to_tuple()
            for quality_check in self.valid_fastqc_file.quality_checks
        ]
        self.assertEqual([('Basic Statistics', 'PASS', 'Equals WARN', 'PASS'), 
                ('Per base sequence quality', 'PASS', 'Equals FAIL', 'PASS'),
                ('Per sequence quality scores', 'PASS', 'Equals FAIL', 'PASS'),
                ('Per base sequence content', 'WARN', 'Equals FAIL', 'PASS'),
                ('Per sequence GC content', 'PASS', 'Equals FAIL', 'PASS'),
                ('Per base N content', 'WARN', 'Equals FAIL', 'PASS'),
                ('Sequence Length Distribution', 'WARN', 'Equals FAIL', 'PASS'),
                ('Sequence Duplication Levels', 'PASS', 'Equals FAIL', 'PASS'),
                ('Overrepresented sequences', 'PASS', 'Equals FAIL', 'PASS'),
                ('Adapter Content', 'PASS', 'Equals FAIL', 'PASS')
            ],
            observed_quality_checks
        )
    
    def test_fastqc_fail(self):
        self.valid_fastqc_file.check(self.fail_conditions)
        first_failed_check = self.valid_fastqc_file.failed_checks[0]

        self.assertEqual('Per tile sequence quality', first_failed_check.metric_name)
        self.assertEqual('FAIL', first_failed_check.metric_value)
        self.assertEqual('Equals FAIL', first_failed_check.check)
        self.assertEqual('FAILURE', first_failed_check.result)
