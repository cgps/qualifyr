from unittest import TestCase
import os, yaml
from qualifyr.bactinspector_file import BactinspectorFile


class TestBactinspectorFile(TestCase):
    def setUp(self):
        good_bactinspector_path = '{0}/test_data/bactinspector_good.tsv'.format(os.path.dirname(os.path.realpath(__file__)))
        uncertain_bactinspector_path = '{0}/test_data/bactinspector_uncertain.tsv'.format(os.path.dirname(os.path.realpath(__file__)))
        self.invalid_bactinspector_path = '{0}/test_data/bactinspector_invalid.tsv'.format(os.path.dirname(os.path.realpath(__file__)))
        self.good_bactinspector_file = BactinspectorFile(good_bactinspector_path)
        self.uncertain_bactinspector_file = BactinspectorFile(uncertain_bactinspector_path)

        pass_conditions_file = '{0}/test_data/pass_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(pass_conditions_file) as pass_conditions_yml:
            self.pass_conditions = yaml.load(pass_conditions_yml)

    def test_bactinspector_validation(self):
        self.assertEqual(9, len(self.good_bactinspector_file.validate()[0]))
        self.assertEqual(9, len(self.good_bactinspector_file.validate()[1]))
    
    def test_bactinspector_invalidation(self):
        with self.assertRaises(Exception): BactinspectorFile(self.invalid_bactinspector_path)

    def test_bactinspector_parsing(self):
        self.assertEqual(
            {
                '%_of_10_best_matches=species': 100,
                'file': 'GCF_000019645.1_ASM1964v1',
                'maximum_genome_length': 6162737,
                'minimmum_genome_length': 3976195,
                'result': 'good',
                'species': 'Escherichia coli',
                'top_hit_distance': 0.0,
                'top_hit_p_value': 0.0,
                'top_hit_shared_hashes': '1000/1000'
            },
            self.good_bactinspector_file.metrics
        )

    def test_bactinspector_pass(self):
        self.good_bactinspector_file.check(self.pass_conditions)
        self.assertEqual('PASS', self.good_bactinspector_file.overall_qc_check_result())

        self.assertEqual(
            [('result', 'good', 'Equals uncertain', 'PASS'), ('species', 'Escherichia coli', 'Equals ', 'PASS')],
            [quality_check.to_tuple() for quality_check in self.good_bactinspector_file.quality_checks]
        )

        self.assertEqual(
            [],
            self.good_bactinspector_file.failed_checks
        )
    
    def test_bactinspector_warning(self):
        self.uncertain_bactinspector_file.check(self.pass_conditions)
        self.assertEqual(
             [('result', 'uncertain', 'Equals uncertain', 'WARNING'), ('species', 'Citrobacter portucalensis', 'Equals ', 'PASS')],
            [quality_check.to_tuple() for quality_check in self.uncertain_bactinspector_file.quality_checks]
        )

        self.assertEqual(
            [('result', 'uncertain', 'Equals uncertain', 'WARNING')],
            [quality_check.to_tuple() for quality_check in self.uncertain_bactinspector_file.failed_checks]
        )
        self.assertEqual('WARNING', self.uncertain_bactinspector_file.overall_qc_check_result())