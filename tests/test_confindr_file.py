from unittest import TestCase
import os, yaml
from qualifyr.confindr_file import ConFindrFile


class TestConfindrFile(TestCase):
    def setUp(self):
        valid_confindr_path = '{0}/test_data/confindr_pass.csv'.format(os.path.dirname(os.path.realpath(__file__)))
        self.invalid_confindr_path = '{0}/test_data/confindr_invalid.csv'.format(os.path.dirname(os.path.realpath(__file__)))
        failed_confindr_path = '{0}/test_data/confindr_fail.csv'.format(os.path.dirname(os.path.realpath(__file__)))
        error_confindr_path = '{0}/test_data/confindr_error.csv'.format(os.path.dirname(os.path.realpath(__file__)))
        self.valid_confindr_file = ConFindrFile(valid_confindr_path)
        self.failed_confindr_file = ConFindrFile(failed_confindr_path)
        self.error_confindr_file = ConFindrFile(error_confindr_path)

        pass_conditions_file = '{0}/test_data/pass_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(pass_conditions_file) as pass_conditions_yml:
            self.pass_conditions = yaml.load(pass_conditions_yml)
        
        fail_conditions_file = '{0}/test_data/fail_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(fail_conditions_file) as fail_conditions_yml:
            self.fail_conditions = yaml.load(fail_conditions_yml)

    def test_confindr_validation(self):
        self.assertEqual(1, len(self.valid_confindr_file.validate()))
    
    def test_confindr_invalidation(self):
        with self.assertRaises(Exception): ConFindrFile(self.invalid_confindr_path)

    def test_confindr_parsing(self):
        self.assertEqual(
            {'contam_status': 'False', 'genus': 'Salmonella', 'num_contam_snvs': 0, 'percentage_contamination': 0},
            self.valid_confindr_file.metrics
        )

    def test_confindr_pass(self):
        self.valid_confindr_file.check(self.pass_conditions)
        self.assertEqual('PASS', self.valid_confindr_file.overall_qc_check_result())
        self.assertEqual(
            [],
            self.failed_confindr_file.quality_checks
        )
    
    def test_confindr_fail(self):
        self.failed_confindr_file.check(self.pass_conditions)
        self.assertEqual(
            [('contam_status', 'True', 'Equals True', 'FAILURE')],
            [quality_check.to_tuple() for quality_check in self.failed_confindr_file.quality_checks]
        )

        self.assertEqual(
            [('contam_status', 'True', 'Equals True', 'FAILURE')],
            [quality_check.to_tuple() for quality_check in self.failed_confindr_file.failed_checks]
        )
        self.assertEqual('FAILURE', self.failed_confindr_file.overall_qc_check_result())

    def test_confindr_fail_extra_condition(self):
        # now with extra condition for failure so that it passes
        self.failed_confindr_file.check(self.fail_conditions)
        self.assertEqual(
            [('contam_status', 'True', 'Equals True', 'FAILURE'), ('percentage_contamination', 8.2, '> 0', 'WARNING')],
            [quality_check.to_tuple() for quality_check in self.failed_confindr_file.quality_checks]
        )

        self.assertEqual(
            [('contam_status', 'True', 'Equals True', 'FAILURE'), ('percentage_contamination', 8.2, '> 0', 'WARNING')],
            [quality_check.to_tuple() for quality_check in self.failed_confindr_file.failed_checks]
        )
        self.assertEqual('WARNING', self.failed_confindr_file.overall_qc_check_result())
    
    def test_confindr_error(self):
        self.error_confindr_file.check(self.pass_conditions)
        self.assertEqual('PASS', self.error_confindr_file.overall_qc_check_result())
        self.assertEqual(
            [],
            self.failed_confindr_file.quality_checks
        )
