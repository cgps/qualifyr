from unittest import TestCase
import os, yaml
from collections import OrderedDict
from qualifyr.quast_file import QuastFile


class TestQuastFile(TestCase):
    def setUp(self):
        valid_quast_path = '{0}/test_data/quast_valid.txt'.format(os.path.dirname(os.path.realpath(__file__)))
        self.invalid_quast_path = '{0}/test_data/quast_invalid.txt'.format(os.path.dirname(os.path.realpath(__file__)))
        self.valid_quast_file = QuastFile(valid_quast_path)

        quast_pass_conditions_file = '{0}/test_data/pass_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(quast_pass_conditions_file) as pass_conditions_yml:
            self.pass_conditions = yaml.load(pass_conditions_yml)

        quast_fail_conditions_file = '{0}/test_data/fail_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(quast_fail_conditions_file) as fail_conditions_yml:
            self.fail_conditions = yaml.load(fail_conditions_yml)

        with open('{0}/test_data/quast_check_output.json'.format(os.path.dirname(os.path.realpath(__file__)))) as f:
            self.json_output = f.read()
        
        with open('{0}/test_data/quast_check_output.tsv'.format(os.path.dirname(os.path.realpath(__file__)))) as f:
            self.tsv_output = f.read()
        
    def test_quast_validation(self):
        self.assertEqual(21, len(self.valid_quast_file.validate()))
    
    def test_quast_invalidation(self):
        with self.assertRaises(Exception): QuastFile(self.invalid_quast_path)

    def test_quast_parsing(self):
        self.assertEqual(
            {'# contigs (>= 0 bp)' : 53, '# contigs (>= 1000 bp)' : 48, '# contigs (>= 5000 bp)' : 36, '# contigs (>= 10000 bp)' : 33, '# contigs (>= 25000 bp)' : 25, '# contigs (>= 50000 bp)' : 17, 'Total length (>= 0 bp)' : 2825352, 'Total length (>= 1000 bp)' : 2822149, 'Total length (>= 5000 bp)' : 2791475, 'Total length (>= 10000 bp)' : 2772446, 'Total length (>= 25000 bp)' : 2626771, 'Total length (>= 50000 bp)' : 2324766, '# contigs' : 53, 'Largest contig' : 339323, 'Total length' : 2825352, 'GC (%)' : 32.67, 'N50' : 146240, 'N75' : 71221, 'L50' : 7, 'L75' : 14, '# N\'s per 100 kbp' : 7.08},
            self.valid_quast_file.metrics
        )

    def test_quast_pass(self):
        self.valid_quast_file.check(self.pass_conditions)
        self.assertEqual([], self.valid_quast_file.failed_checks)
         # check each result (all checks)
        first_check = self.valid_quast_file.quality_checks[0]
        self.assertEqual('# contigs (>= 1000 bp)', first_check.metric_name)
        self.assertEqual(48, first_check.metric_value)
        self.assertEqual('> 70', first_check.check)
        self.assertEqual('PASS', first_check.result)

        second_check = self.valid_quast_file.quality_checks[1]
        self.assertEqual('N50', second_check.metric_name)
        self.assertEqual(146240, second_check.metric_value)
        self.assertEqual('< 100000', second_check.check)
        self.assertEqual('PASS', second_check.result)

         # check each result (failed checks)
        self.assertEqual(
            [],
            self.valid_quast_file.failed_checks
        )

        self.assertEqual({'quast':  OrderedDict()}, self.valid_quast_file.quality_checks_to_dict())

        self.assertEqual('PASS', self.valid_quast_file.overall_qc_check_result())

    def test_quast_fail(self):
        self.valid_quast_file.check(self.fail_conditions)
        # check each result (all checks)
        first_check = self.valid_quast_file.quality_checks[0]
        self.assertEqual('# contigs (>= 1000 bp)', first_check.metric_name)
        self.assertEqual(48, first_check.metric_value)
        self.assertEqual('> 40', first_check.check)
        self.assertEqual('WARNING', first_check.result)

        second_check = self.valid_quast_file.quality_checks[1]
        self.assertEqual('N50', second_check.metric_name)
        self.assertEqual(146240, second_check.metric_value)
        self.assertEqual('< 150000', second_check.check)
        self.assertEqual('WARNING', second_check.result)

         # check each result (failed checks)
        first_check = self.valid_quast_file.failed_checks[0]
        self.assertEqual('# contigs (>= 1000 bp)', first_check.metric_name)
        self.assertEqual(48, first_check.metric_value)
        self.assertEqual('> 40', first_check.check)
        self.assertEqual('WARNING', first_check.result)

        second_check = self.valid_quast_file.failed_checks[1]
        self.assertEqual('N50', second_check.metric_name)
        self.assertEqual(146240, second_check.metric_value)
        self.assertEqual('< 150000', second_check.check)
        self.assertEqual('WARNING', second_check.result)

        self.assertEqual({
                        'quast': OrderedDict([
                                    ('# contigs (>= 1000 bp)',
                                        OrderedDict([
                                            ('metric_value', 48),
                                            ('check', '> 40'),
                                            ('check_result', 'WARNING')
                                        ])
                                    ),
                                    ('N50',
                                        OrderedDict([
                                            ('metric_value', 146240),
                                            ('check', '< 150000'),
                                            ('check_result', 'WARNING')
                                        ])
                                    )
                                ])
                        },
                        self.valid_quast_file.quality_checks_to_dict()
                    )

        self.assertEqual('WARNING', self.valid_quast_file.overall_qc_check_result())

    def test_quast_check_output(self):
        self.valid_quast_file.check(self.fail_conditions)
        self.assertEqual (
            self.tsv_output,
            self.valid_quast_file.output_quality_checks_string()
        )
        self.assertEqual (
            self.json_output,
            self.valid_quast_file.output_quality_checks_string('json')
        )





