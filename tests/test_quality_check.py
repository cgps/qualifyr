from unittest import TestCase
import os, yaml
from collections import OrderedDict
from qualifyr.quality_check import QualityCheck


class TestQuastFile(TestCase):
    def setUp(self):
        pass_conditions_file = '{0}/test_data/pass_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(pass_conditions_file) as pass_conditions_yml:
            self.pass_conditions = yaml.load(pass_conditions_yml)

        fail_conditions_file = '{0}/test_data/fail_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(fail_conditions_file) as fail_conditions_yml:
            self.fail_conditions = yaml.load(fail_conditions_yml)

        test_conditions_file = '{0}/test_data/test_conditions.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        with open(test_conditions_file) as test_conditions_yml:
            self.test_conditions = yaml.load(test_conditions_yml)


    def test_check_metric_pass(self):
        quast_n50_check = QualityCheck('N50', 150000, self.pass_conditions['quast'])
        self.assertEqual('PASS', quast_n50_check.result)
        self.assertEqual('< 100000', quast_n50_check.check)

        fastqc_per_base_sequence_content_check = QualityCheck('Per base sequence content', 'PASS', self.pass_conditions['fastqc'])
        self.assertEqual('PASS', fastqc_per_base_sequence_content_check.result)
        self.assertEqual('Equals FAIL', fastqc_per_base_sequence_content_check.check)

        confindr_contam_status_check = QualityCheck('contam_status', 'False', self.pass_conditions['confindr'])
        self.assertEqual('PASS', confindr_contam_status_check.result)
        self.assertEqual('Equals True', confindr_contam_status_check.check)

    
    def test_check_metric_warning(self):
        quast_n50_check = QualityCheck('N50', 75000, self.pass_conditions['quast'])
        self.assertEqual('WARNING', quast_n50_check.result)
        self.assertEqual('< 100000', quast_n50_check.check)

        quast_n50_check = QualityCheck('N50', 75000, self.test_conditions['quast'])
        self.assertEqual('WARNING', quast_n50_check.result)
        self.assertEqual('> 50000 and < 150000', quast_n50_check.check)

        

    def test_check_metric_failure(self):
        quast_n50_check = QualityCheck('N50', 40000, self.pass_conditions['quast'])
        self.assertEqual('FAILURE', quast_n50_check.result)
        self.assertEqual('< 50000', quast_n50_check.check)

        fastqc_per_base_sequence_content_check = QualityCheck('Per base sequence content', 'FAIL', self.pass_conditions['fastqc'])
        self.assertEqual('FAILURE', fastqc_per_base_sequence_content_check.result)
        self.assertEqual('Equals FAIL', fastqc_per_base_sequence_content_check.check)

        fastqc_per_base_sequence_content_check = QualityCheck('Per base sequence quality', 'FAIL', self.test_conditions['fastqc'])
        self.assertEqual('FAILURE', fastqc_per_base_sequence_content_check.result)
        self.assertEqual('One of WARNING, FAIL', fastqc_per_base_sequence_content_check.check)

        fastqc_per_base_sequence_content_check = QualityCheck('Per base sequence quality', 'WARNING', self.test_conditions['fastqc'])
        self.assertEqual('FAILURE', fastqc_per_base_sequence_content_check.result)
        self.assertEqual('One of WARNING, FAIL', fastqc_per_base_sequence_content_check.check)

        fastqc_per_base_sequence_content_check = QualityCheck('Per base N content', 'WARNING', self.test_conditions['fastqc'])
        self.assertEqual('FAILURE', fastqc_per_base_sequence_content_check.result)
        self.assertEqual('Does not equal PASS', fastqc_per_base_sequence_content_check.check)


        confindr_contam_status_check = QualityCheck('contam_status', 'True', self.pass_conditions['confindr'])
        self.assertEqual('FAILURE', confindr_contam_status_check.result)
        self.assertEqual('Equals True', confindr_contam_status_check.check)

    def test_to_tuple(self):
        quast_n50_check = QualityCheck('N50', 150000, self.pass_conditions['quast'])
        self.assertEqual(('N50', 150000, '< 100000', 'PASS'), quast_n50_check.to_tuple())
